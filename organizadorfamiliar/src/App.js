import React from 'react';
import logo from './assets/images/logo.svg';
import './assets/css/App.css';

//Importar componentes
import MiComponente from './components/MiComponente'

function HolaMundo(nombre, edad){
  let presentacion = <div>
    <h2>Hola, soy {nombre}</h2>
    <h3>Tengo {edad} años</h3>
  </div>
  return presentacion;
}

function App() {
  let nombre="Manolo Lamas";
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          3.14159265353979323846
        </p>
        {HolaMundo(nombre, 12)}
      </header>

      <section className="componentes">

        <MiComponente/>

      </section>

    </div>
  );
}

export default App;
